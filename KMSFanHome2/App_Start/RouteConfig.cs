﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using KMSFanHome2.Services;


namespace KMSFanHome2
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            ////自己添加的方法
            //routes.MapElegantRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);



  
            //系统的方法
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
      name: "Test",
      url: "{controller}/{action}",
      defaults: new { controller = "Test", action = "UsingViewData" }
      );

            //投票路由
            routes.MapRoute(
                name: "Vote",
                url: "{controller}/{action}/{username}",
                defaults: new { controller = "Vote", action = "VoteUser",username=UrlParameter.Optional }
                );

        }
    }
}