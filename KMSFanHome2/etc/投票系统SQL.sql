
--投票结果统计表
create table count_voting
(
selectname varchar(100), --被投票项的名称
TypeName varchar(100), -- 投票的类别
CountVote int --投票的票数的总和

)

alter table count_voting alter column selectname varchar(100) not null -- 设置非空约束
alter table count_voting add constraint pk_count_voting primary key(selectname) --设置主键约束

--投票人记录表
create table ip_vote
(
id varchar(100), --唯一标识
ip varchar(20), --投票人的IP地址
Location varchar(50), --投票人所处的地理位置
VoteTime varchar(50), --投票时间
SelectName varchar(50) -- 选择项

)

alter table ip_vote alter column id varchar(100) not null -- 设置非空约束
alter table ip_vote add constraint id primary key(id) --设置主键约束

--用户表
create table users
(
username varchar(100),
passwd varchar(100) 

)

alter table users alter column username varchar(100) not null -- 设置非空约束
alter table users add constraint pk_username primary key(username) --设置主键约束




create table vote_details
(
voteId int primary key,  --投票ID
memo varchar(200),		--投票说明
vtime varchar(200) --投票时间

)
