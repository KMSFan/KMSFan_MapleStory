﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KMSFanHome2.Models;
using System.Collections;

namespace KMSFanHome2.Services
{

    /// <summary>
    /// 投票系统服务层
    /// </summary>
    public class VoteServices:IVoteServices
    {
        /// <summary>
        /// 添加投票
        /// </summary>
        public void AddVote()
        { 

        
        }

        /// <summary>
        /// 查询投票
        /// </summary>
        //public void QueryVote(VoteModels model)
        //{

        
        //}

        /// <summary>
        /// 删除投票
        /// </summary>
        public void DeleteVote()
        { 
        
        
        }

        /// <summary>
        /// 更新投票
        /// </summary>
        public void UpdateVote()
        { 
        
        }

        /// <summary>
        /// 查询用户
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>
        public List<Test.users> QueryUsers(string username)
        {
            List<Test.users> uList = new List<Test.users>();

            //Linq to Sql    
            Test.TestDataContext dc = new Test.TestDataContext();

            //查询语句
            IQueryable<Test.users> query = from p in dc.users
                                           where p.username.Contains(username)
                                           select p;

            foreach (var u in query)
            {
                uList.Add(u);
            
            }

            return uList;
        }
        

    }
}