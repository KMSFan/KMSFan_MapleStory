﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace KMSFanHome2.Services
{
    /// <summary>
    /// 处理字符串操作的静态类
    /// </summary>
   internal static class StringElegantHelper
    {
       public static readonly char minus = '-';

       public static string Elegant(string s)
       {
           var builder = new StringBuilder();
           var index = 0;

           foreach (var c in s)
           {
               if (c >='A' && c <= 'Z')
               {
                   if (index > 0)
                   {
                       builder.Append(minus);
                   }
                   builder.Append(char.ToLower(c));
               }
               else if (c == minus)
               {
                   builder.Append(minus);
                   builder.Append(minus);

               }

               else
               {
                   builder.Append(c);
               }

               index++;
           
           }
           return builder.ToString();
       }
    }
}