﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KMSFanHome2.Models;

namespace KMSFanHome2.Services
{
    /// <summary>
    /// 投票系统的接口
    /// </summary>
   public interface IVoteServices
    {
        void AddVote(); /*添加一个新的投票*/
        //void QueryVote(VoteModels models); /*查询统计的票数*/
        void DeleteVote();/*删除投票*/
        void UpdateVote(); /*更新投票内容*/
        List<Test.users> QueryUsers(string username); /*查询用户*/
    }
}
