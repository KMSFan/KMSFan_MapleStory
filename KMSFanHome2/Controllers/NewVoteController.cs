﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KMSFanHome2.Models;

namespace KMSFanHome2.Controllers
{
    public class NewVoteController : Controller
    {
        VotedatabaseContext vc = new VotedatabaseContext(); /*code first实例*/

        //
        // GET: /NewVote/

        /// <summary>
        /// 新投票系统首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {

            /*多表查询*/
            var temp = from u in vc.count_voting
                       join uu in vc.vote_details on u.VoteTypeId equals uu.voteId
                       select new
                       {
                           name = uu.memo,
                           voteItem = u.selectname
                       };

            //var list = new List<ModelList>() { 
                
            //};

            /*单表查询*/
            var singleTemp = from u in vc.vote_details
                             select new {memo=u.memo };


            List<ModelListComplex> list = new List<ModelListComplex>();
            //foreach (var i in temp)
            //{

            //    ModelListComplex mlc = new ModelListComplex();
               
            //    mlc.voteName = i.name;

            //    if (mlc.voteDetail == null)
            //    {
            //        mlc.voteDetail = new List<string>();
            //    }

            //    if (i.voteItem != null)
            //    {
            //        //先新建一个实例
                    
            //        mlc.voteDetail.Add(i.voteItem);
            //    }


            //        list.Add(mlc);
   
            //}

            /*解决了一个投票项名称，多个子投票项的问题*/
            foreach (var i in singleTemp)
            {
                ModelListComplex mlc = new ModelListComplex();

                mlc.voteName = i.memo;

                var temp2 = from u in vc.count_voting
                           
                           join uu in vc.vote_details on u.VoteTypeId equals uu.voteId
                           where u.TypeName == i.memo
                           select new
                           {
                               name = uu.memo,
                               voteItem = u.selectname
                           };

                foreach (var j in temp2)
                {
                    if (mlc.voteDetail == null)
                    {
                        mlc.voteDetail = new List<string>();
                    }
               
                    mlc.voteDetail.Add(j.voteItem);
                
                }

                list.Add(mlc);

            
            }

            return View(list);
        }

        /// <summary>
        /// 增加投票项
        /// </summary>
        /// <returns></returns>
        public ActionResult AddVote()
        {
            return View();
        
        }


        /// <summary>
        /// 新增投票,建立一个父模型，里面包含了2个子模型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddVote(Models.ModelList d)
        {
            //投票详情表
            vote_details vt = new vote_details() { 
            memo= d.vd.memo,
            isSingle=d.vd.isSingle,

            
            };
            vc.vote_details.Add(vt);
            vc.SaveChanges(); /*有2此保存，第一次保存是保存上面的*/

            //通过上次保存的结果生成voteid，然后插入到下一张表去
            Models.vote_details voteList=  vc.vote_details.Where(p => p.memo == d.vd.memo).FirstOrDefault();

            if (voteList != null)
            {
                //投票具体项表

                foreach(var c in d.cv)
                {
                    count_voting cv = new count_voting()
                    {
                        selectname = c.selectname,
                        VoteTypeId = voteList.voteId,
                        TypeName = voteList.memo
                    };

                    vc.count_voting.Add(cv);

                    vc.SaveChanges();
                
                
                }


            
            }

            return RedirectToAction("Index", "Home"); /*回到最初的原始界面*/
        
        }

    }
}
