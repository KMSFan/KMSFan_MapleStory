﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KMSFanHome2.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UsingViewData()
        {
            ViewData["Title"] = "ABC";
            ViewData["ProjectName"] = "DEF";
            Dictionary<string, string> stackholder = new Dictionary<string, string>();
            stackholder.Add("Client", "A");
            stackholder.Add("Client2", "AB");
            ViewData["stackholder"] = stackholder;

            List<string> module = new List<string>();
            module.Add("1");
            module.Add("2");
            ViewData["module"] = module;

            return View();
        
        }

    }
}
