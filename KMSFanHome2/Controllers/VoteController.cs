﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KMSFanHome2.Services;
using KMSFanHome2.Models;
using System.Data.Entity;
using System.Data;

namespace KMSFanHome2.Controllers
{
    /// <summary>
    /// 投票系统的控制器
    /// </summary>
  
    public class VoteController : Controller
    {
        VotedatabaseContext vc=new VotedatabaseContext();

        /// <summary>
        /// 接口声明
        /// </summary>
        public IVoteServices voteService { get; set; }

        //
        // GET: /Vote/

        public ActionResult Index()
        {
            var list = vc.users.ToList();

            return View(list);
        }


        /// <summary>
        /// 获得用户列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetUsersList()
        {


            return this.View();
        }

        public ActionResult VoteUser()
        {
          
            return this.View();
        
        }

        /// <summary>
        /// 增加投票
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult VoteUser(Models.user u)
        { 

            //检查用户名是否重复
            var check_username = vc.users.Where(p => p.username == u.username).FirstOrDefault();

            if (check_username != null)
            {
                ModelState.AddModelError("username", "用户名已经被注册！");
                return View();
            }
            else
            {
                //var member=vc.users.Where
                user ur = new user()
                {
                    username = u.username,
                    passwd = u.passwd

                };
                vc.users.Add(ur);
                vc.SaveChanges();
                return RedirectToAction("Index", "Home");
            }


        
        }
        public ActionResult DeleteUser(int  id=0)
        {
            //var check_user = vc.users.Where(p => p.username == username).SingleOrDefault();
            //if (check_user != null)
            //{
            //    vc.users.Remove(check_user); //删除用户
            //    vc.SaveChanges();
            //    return View();

            //}
            //else
            //{
            //    ModelState.AddModelError("username", "该用户不存在");
            //    return View();
            
            //}
            Models.user u = vc.users.Find(id);
            if (u == null)
            {

                return HttpNotFound();
            }
            else
            {
                vc.users.Remove(u);
                vc.SaveChanges();
                return RedirectToAction("Index");
            
            }
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns></returns>
        public ActionResult Edit(int id = 0)
        {
            Models.user u = vc.users.Find(id);

            if (u == null)
            {
                return HttpNotFound();
            }

            return View(u);
        
        }

        [HttpPost]
        public ActionResult Edit(Models.user u)
        {
            if (ModelState.IsValid)
            {
                vc.Entry(u).State = EntityState.Modified;
                vc.SaveChanges();
                return RedirectToAction("Index");
            
            }

            return View(u);
        
        }



        
    }
}
