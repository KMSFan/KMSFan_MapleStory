﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KMSFanHome2.Models
{

    /// <summary>
    /// 父模型
    /// </summary>
    public class ModelList
    {
        public vote_details vd { get; set; }
        //public count_voting cv { get; set; }

        public List<count_voting> cv { get; set; }

    }

    //包含了投票名称和投票项（多项）
    public class ModelListComplex
    {
        public string voteName { get; set; }
        public List<string> voteDetail { get; set; }
    }
}