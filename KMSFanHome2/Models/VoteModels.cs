﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace KMSFanHome2.Models
{


    /// <summary>
    /// 投票结果统计表
    /// </summary>
    [Table(Name = "Count_voting")]
    public class Count_voting
    {
        [Column(IsPrimaryKey=true)]
        public string SelectName { get; set; } /*被投票项的名称*/

        public string TypeName { get; set; } /*投票的类别*/
        public string CountVote { get; set; } /*投票的票数的总和*/
    }

    /// <summary>
    /// 投票人记录表
    /// </summary>
    [Table(Name = "Ip_vote")]
    public class Ip_vote
    {
        [Column(IsPrimaryKey=true)]
        public string Id { get; set; }  /*唯一标识*/

        public string Ip { get; set; }  /*投票人的IP地址*/
        public string Location { get; set; } /*投票人所处的地理位置*/
        public string VoteTime { get; set; } /*投票时间*/
        public string SelectName { get; set; } /*选择项*/
    
    }

    /// <summary>
    /// 用户表
    /// </summary>
    [Table(Name="users")]
    public class Users
    {
        [Column(IsPrimaryKey=true,DbType="varchar(100)")]
        public string UserName { get; set; } /*用户名*/
        public string Password { get; set; } /*密码*/
    
    }


}