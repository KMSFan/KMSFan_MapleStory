using System;
using System.Collections.Generic;

namespace KMSFanHome2.Models
{
    public partial class user
    {
        public string username { get; set; }
        public string passwd { get; set; }
        public string userpicpath { get; set; }
        public int id { get; set; }
        public Nullable<int> votecount { get; set; }

    }
}
