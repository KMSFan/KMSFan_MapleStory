using System;
using System.Collections.Generic;

namespace KMSFanHome2.Models
{
    public partial class ip_vote
    {
        public string id { get; set; }
        public string ip { get; set; }
        public string Location { get; set; }
        public string VoteTime { get; set; }
        public string SelectName { get; set; }
        public int VoteId { get; set; }
    }
}
