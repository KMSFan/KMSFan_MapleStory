using System;
using System.Collections.Generic;

namespace KMSFanHome2.Models
{
    public partial class count_voting
    {
        public string selectname { get; set; }
        public string TypeName { get; set; }   //投票的类型名称
        public Nullable<int> CountVote { get; set; }
        public int VoteTypeId { get; set; }  /*投票的类型ID*/
    }
}
