using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KMSFanHome2.Models.Mapping
{
    public class count_votingMap : EntityTypeConfiguration<count_voting>
    {
        public count_votingMap()
        {
            // Primary Key
            this.HasKey(t => t.selectname);

            // Properties
            this.Property(t => t.selectname)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.TypeName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("count_voting");
            this.Property(t => t.selectname).HasColumnName("selectname");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.CountVote).HasColumnName("CountVote");
        }
    }
}
