using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KMSFanHome2.Models.Mapping
{
    public class vote_detailsMap : EntityTypeConfiguration<vote_details>
    {
        public vote_detailsMap()
        {
            // Primary Key
            this.HasKey(t => t.voteId);

            // Properties
            this.Property(t => t.voteId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity); /*������ʾֵIDENTITY*/

            this.Property(t => t.memo)
                .HasMaxLength(200);

            this.Property(t => t.vtime)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("vote_details");
            this.Property(t => t.voteId).HasColumnName("voteId");
            this.Property(t => t.memo).HasColumnName("memo");
            this.Property(t => t.vtime).HasColumnName("vtime");
        }
    }
}
