using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KMSFanHome2.Models.Mapping
{
    public class userMap : EntityTypeConfiguration<user>
    {
        public userMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.username)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.passwd)
                .HasMaxLength(100);

            this.Property(t => t.userpicpath)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("users");
            this.Property(t => t.username).HasColumnName("username");
            this.Property(t => t.passwd).HasColumnName("passwd");
            this.Property(t => t.userpicpath).HasColumnName("userpicpath");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.votecount).HasColumnName("votecount");
        }
    }
}
