using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KMSFanHome2.Models.Mapping
{
    public class ip_voteMap : EntityTypeConfiguration<ip_vote>
    {
        public ip_voteMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.ip)
                .HasMaxLength(20);

            this.Property(t => t.Location)
                .HasMaxLength(50);

            this.Property(t => t.VoteTime)
                .HasMaxLength(50);

            this.Property(t => t.SelectName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ip_vote");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ip).HasColumnName("ip");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.VoteTime).HasColumnName("VoteTime");
            this.Property(t => t.SelectName).HasColumnName("SelectName");
        }
    }
}
