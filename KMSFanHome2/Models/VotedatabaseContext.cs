using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using KMSFanHome2.Models.Mapping;

namespace KMSFanHome2.Models
{
    public partial class VotedatabaseContext : DbContext
    {
        static VotedatabaseContext()
        {
            Database.SetInitializer<VotedatabaseContext>(null);
        }

        public VotedatabaseContext()
            : base("Name=VotedatabaseContext")
        {
        }

        public DbSet<count_voting> count_voting { get; set; }
        public DbSet<ip_vote> ip_vote { get; set; }
        public DbSet<user> users { get; set; }
        public DbSet<vote_details> vote_details { get; set; }

        //�½�ģ��


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new count_votingMap());
            modelBuilder.Configurations.Add(new ip_voteMap());
            modelBuilder.Configurations.Add(new userMap());
            modelBuilder.Configurations.Add(new vote_detailsMap());
        }
    }
}
